=======
CHANGES
=======


Unreleased
==========

- Nothing yet

0.6.1 (2013-10-08)
==================

- Include CHANGES.txt in release.


0.6.0 (2013-10-08)
==================

- Add `domain`, `max-age`, and `path` configuration options.

0.5.1 (2013-06-12)
==================

Open-source release.

0.5 (2013-03-12)
================

- Use a cryptographically secure random number source (os.urandom) for
  generating browser ids.
- Fix a bug in the get/remove helpers that caused SessionData objects
  to be created unnecessarily.

0.4 (2012-01-03)
================

- Accept a database name parameter for session storage.


0.3 (2011-11-11)
================

- Put arguments to helper functions in a more logical order.
- Require pkg_id to discourage bad use pattern.


0.2 (2011-11-10)
================

- Make http-only and secure configurable.
- Test configuration options.
- Test database initialization and options.


0.1 (2011-11-10)
================

Initial release
